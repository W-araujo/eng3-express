const express = require('express')
const app = express()

app.use(express.json())

let clients = [
    { id: 3, name: 'Débora Reis', phone: '5398543456' },
    { id: 1, name: 'Ramirez Souza', phone: '5398544456' },
    { id: 2, name: 'Matheus Conrrado', phone: '5398545456' },
    { id: 4, name: 'Miriam Martins', phone: '5398546456' },
]

function log(request, response, next) {
    const { url, method } = request;
    console.log(`${method} - ${url} at ${new Date()}`)
    return next()
}

app.use(log)

app.get('/clients', (request, response) => response.status(200).json(clients))

app.get('/clients/:id', (request, response) => {
    const { id } = request.params
    const client = clients.find(value => value.id == id)
    if (client == undefined) {
        return response.status(400).json({ error: 'Falha na requisição' });
    } else {
        return response.status(200).json(client)
    }
})

app.post('/clients', (request, response) => {
    const client = request.body
    clients.push(client)
    return response.status(201).json({ message: "Registrado com sucesso", client })
})

app.put('/clients/:id', (request, response) => {
    const id = request.params.id
    const name = request.body.name


    let client = clients.find(value => value.id == id)

    if (client == undefined) {
        return response.status(400).send()
    } else {
        client.name = name
        return response.status(200).json(client)
    }
})

app.delete('/clients/:id', (request, response) => {
    const { id } = request.params
    const index = clients.findIndex(value => value.id == id)

    if (index == -1) {
        return response.status(400).send()
    } else {
        clients.splice(index, 1)
        return response.status(204).json({ meessage: "Deletado com sucesso" })
    }
})

app.listen(3333)